import { render } from 'preact'
import { App } from './app.tsx'
import { Test } from './test.tsx'
import { Route, Router } from 'preact-router'
import { QuirkTest } from './quirker'
import './index.css'

const Main = () => (<Router>
    <Route path="/" component={App} />
    <Route path="/test" component={Test} />
    <Route path="/quirk-test" component={QuirkTest} />
</Router>);


render(<Main />, document.getElementById('app')!)
