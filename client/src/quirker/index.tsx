import './style.css'
import { Component, JSX } from 'preact'
import { Effects } from './effects';



interface Error {
    err: 'failure' | 'eof'
    msg?: string
}

function isError(i: any): i is Error {
    return typeof i === "object" && typeof i.err === "string"
}

function printError(e: Error) {
    if (e.msg) {
        return e.msg;
    } else {
        switch (e.err) {
            case 'failure':
                return 'Something failed.';
            case 'eof':
                return 'Ran out of data to process.';
        }
    }
}

type Parser<T> = (text: string) => [string, T] | Error;

type Token = { kind: 'quote' } | { kind: 'string' | 'space', value: string };

function subToken(quirk: string): [string, Token] | Error {
    switch (quirk.charAt(0)) {
        case '':
            return { err: "eof" };
        case ' ':
        case '\t':
        case '\n':
        case '\r':
            for (let i = 1; i < quirk.length; i++) {
                switch (quirk[i]) {
                    case ' ':
                    case '\t':
                    case '\n':
                    case '\r':
                        break;
                    default:
                        return [quirk.substring(i), { kind: 'space', value: quirk.substring(0, i) }]
                }
            }
            return ["", { kind: 'space', value: quirk }];
        case '"':
            return [quirk.substring(1), { kind: 'quote' }];
        case '\\':
            let char = quirk.charAt(1);
            if (char === "") {
                return { err: 'failure', msg: "String ends after forward slash." }
            } else {
                return [quirk.substring(char.length + 1), { kind: 'string', value: char }]
            }
        default:
            let index = -1;
            [' ', '\n', '\t', '\r', '"', '\\'].forEach(element => {
                let curr_index = quirk.indexOf(element);
                if (index === -1 || (curr_index !== -1 && curr_index < index)) {
                    index = curr_index;
                }
            });
            if (index === -1) {
                return ["", { kind: 'string', value: quirk }]
            } else {
                return [quirk.substring(index), { kind: 'string', value: quirk.substring(0, index) }]
            }
    }
}

function parseToken(quirk: string): [string, string] | Error {
    let p1 = subToken(quirk);
    if (isError(p1)) {
        return p1;
    }
    let [rest, first] = p1;
    switch (first.kind) {
        case 'space':
            return parseToken(rest);
        case 'quote':
            {
                let buffer = "";
                while (true) {
                    let p2 = subToken(rest);
                    if (isError(p2)) {
                        return p2;
                    }
                    let [r, t] = p2;
                    rest = r;
                    switch (t.kind) {
                        case 'string':
                        case 'space':
                            buffer += t.value;
                            break;
                        case 'quote':
                            let p3 = subToken(rest);
                            if (isError(p3)) {
                                switch (p3.err) {
                                    case 'eof':
                                        return [rest, buffer];
                                    default:
                                        return p3;
                                }
                            }
                            let [r2, t2] = p3;
                            rest = r2;
                            switch (t2.kind) {
                                case 'quote':
                                    return { err: 'failure', msg: "Quote cannot immediatly follow another quote." }
                                case 'string':
                                    return { err: 'failure', msg: "String continues after quote." }
                                case 'space':
                                    console.log("Returnin '"+buffer+"'");
                                    return [rest, buffer];
                            }
                    }
                }
            }
        case 'string':
            {
                let buffer = first.value;
                while (true) {
                    let p2 = subToken(rest);
                    if (isError(p2)) {
                        switch (p2.err) {
                            case 'eof':
                                return ["", buffer];
                            default:
                                return p2;
                        }
                    }
                    let [r, t] = p2;
                    rest = r;
                    switch (t.kind) {
                        case 'space':
                            return [rest, buffer]
                        case 'quote':
                            return { err: 'failure', msg: 'Quotes cannot be put in space deliminated tokens.' };
                        case 'string':
                            buffer += t.value;
                            break;
                    }
                }
            }
    }
}

export type StackNode =
    string |
    { kind: 'styled', style: JSX.AllCSSProperties, children: StackNode[] } |
    { kind: 'quote' | 'spoiler', name?: string, children: StackNode[] } |
    { kind: 'code', languate?: string, content: string, children: undefined } |
    { kind: 'image', url: string, alt?: string, dimensions?: [number, number], children: undefined };

function renderStack(nodes: StackNode[]): JSX.Element {
    if (nodes.length === 0) {
        return <></>
    } else if (nodes.length === 1) {
        let node = nodes[0];
        if (typeof node === "string") {
            return <>{node}</>
        } else {
            switch (node.kind) {
                case 'styled':
                    return <span style={node.style}>{renderStack(node.children)}</span>
                case 'image':
                    if (node.dimensions) {
                        return <img src={node.url} alt={node.alt} width={node.dimensions[0]} height={node.dimensions[1]} />;
                    } else {
                        return <img src={node.url} alt={node.alt} />;
                    }
                default:
                    throw "Rendering not implemented for " + node.kind + "\nThis is a bug that needs to be fixed.";
            }
        }
    } else {
        let render_vec = [];
        let node = undefined;
        while (node = nodes.pop()) {
            render_vec.push(renderStack([node]));
        }
        return <>{render_vec}</>
    }
}

export type StackEffect = (nodes: StackNode[]) => StackNode[] | string;

const DEBUG_COLORS = ["blue", "yellow", "red", "green"];

function wrapInDebugNodes(nodes: StackNode[], offset: number = 0): StackNode[] {
    switch (nodes.length) {
        case 0:
            return [];
        case 1:
            {
                let node = nodes[0];
                if (typeof node === 'object' && node.children) {
                    node.children = wrapInDebugNodes(node.children, offset);
                }
                let color = DEBUG_COLORS[offset];
                let underlined: StackNode = {
                    kind: "styled",
                    style: {
                        borderBottomWidth: "2px",
                        borderBottomStyle: "solid",
                        borderBottomColor: color,
                        paddingBottom: "1px",
                        display:"inline-block"
                    },
                    children: [node]
                };
                return [underlined];
            }
        default:
            return nodes.map((node, index) => wrapInDebugNodes([node], (offset + index) % DEBUG_COLORS.length)[0]);
    }
}

export type EffectConstructor = (nodes: (StackNode | StackEffect)[]) => void | string;

export interface Effect {
    name: string,
    constructor: EffectConstructor,
}

interface QuirkTestState {
    text: string,
    quirk: string,
    debug: boolean
}

export class QuirkTest extends Component<{}, QuirkTestState> {
    constructor() {
        super();
        this.state = { quirk: "", text: "", debug: false };
    }

    updateText = (e: JSX.TargetedEvent<HTMLTextAreaElement, Event>) => {
        this.setState({ text: e.currentTarget.value })
    }

    updateQuirk = (e: JSX.TargetedEvent<HTMLTextAreaElement, Event>) => {
        this.setState({ quirk: e.currentTarget.value })
    }

    updateDebug = (e: JSX.TargetedEvent<HTMLInputElement, Event>) => {
        this.setState({ debug: e.currentTarget.checked })
    }

    render({ }, { text, quirk, debug }: QuirkTestState) {
        let vec: (string | StackEffect)[] | string = [];
        let q = quirk;
        tokenizer:
        while (q !== "") {
            let poss_token = parseToken(q);
            if (isError(poss_token)) {
                vec = printError(poss_token);
                break tokenizer;
            } else {
                let [rest, token] = poss_token;
                q = rest;
                let construct = Effects[token];
                if (construct) {
                    let poss_error = construct(vec);
                    if (poss_error) {
                        vec = poss_error;
                        break tokenizer;
                    }
                } else {
                    vec.push(token);
                }
            }
        }

        let commands: StackEffect[] | string = [];
        let spare: [number, string][] = [];
        // Verify all nodes in the vec are Stack effects
        // There should be no more plain arguments in it.
        if (typeof vec === 'string') {
            commands = vec;
        } else {
            for (let i = 0; i < vec.length; i++) {
                let element = vec[i];
                if (typeof element === 'string') {
                    spare.push([i, element]);
                } else {
                    commands.push(element);
                }
            }
        }

        let nodes: StackNode[] | string = [text];
        let length;
        let result;
        if (typeof commands === "string") {
            result = <>Error: {commands}</>
        } else {
            for (let i = 0; i < commands.length; i++) {
                let element = commands[i];
                let res = element(nodes);
                if (typeof res === 'string') {
                    nodes = res;
                    break;
                } else {
                    nodes = res;
                }
            }
            if (typeof nodes === 'string') {
                result = <>Error: {nodes}</>
            } else {
                length = nodes.length;
                let renderNodes: StackNode[];
                if (debug) {
                    renderNodes = wrapInDebugNodes(nodes);
                } else {
                    renderNodes = nodes;
                }
                result = renderStack(renderNodes);
            }
        }

        let unconsumed = <></>;
        if (spare.length !== 0) {
            let unconsumed_inputs: string[] = [];
            spare.forEach(element => {
                unconsumed_inputs.push(`${element[0]}: ${element[1]}`);
            })
            let list = unconsumed_inputs.join(', ');
            unconsumed = <div>
                <h3>UNCONSUM3D</h3>
                [{list}]
            </div>;
        }

        return (
            <div class="bl4r">
                <h2>OUTPUT</h2>
                <div>{result}</div>
                {unconsumed}
                <h2>T3XT {length}</h2>
                <textarea value={text} onInput={this.updateText} />
                <h2>QU1RK</h2>
                <textarea value={quirk} onInput={this.updateQuirk} />
                <br />
                D3BUG: <input type="checkbox" checked={debug} onInput={this.updateDebug} />
                <p style={{ color: "#008282", textAlign: "left" }}>
                    HOW 1T WORKS <br />
                    QU1RKS 4R3 BU1LT 4S 4 S3R13S OF COMM4NDS <br />
                    4RGUM3NTS 4R3 ON TH3 L3FT <br />
                    <br />
                    COMM4NDS <br />
                    [COLORT3XT] color <br />
                    ['upper'|'lower'] case <br />
                    [a] [b] replace <br />
                    R3PL4C3S A W1TH B
                </p>
            </div>
        )
    }
}
