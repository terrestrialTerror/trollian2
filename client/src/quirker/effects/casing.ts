import { Effect, StackEffect } from "..";
import { stringReplacer } from "./util";

function casing(kind: 'upper' | 'lower'): StackEffect {
    let map: (text: string) => string;
    switch (kind) {
        case 'upper':
            map = text => text.toUpperCase();
            break;
        case 'lower':
            map = text => text.toLowerCase();
            break;
    }

    return stringReplacer(map);
}

const Casing: Effect = {
    name: "case",
    constructor: vec => {
        let arg = vec.pop();
        if (arg !== undefined) {
            if (arg === 'upper' || arg === 'lower') {
                vec.push(casing(arg));
            } else {
                return "Casing must either be upper or lower.";
            }
        } else {
            return "Not enough arguments for replace.";
        }
    }
}

export default Casing;