import { Effect, EffectConstructor } from '..';
import Color from "./color";
import Replace from './replace';
import Append from './append';
import Casing from './casing';
import Prepend from './prepend';
import SplitBy from './splitBy';

var EffectList: Effect[] = [
    Color,
    Casing,
    Replace,
    Append,
    Prepend,
    SplitBy
];

var Effects: { [key: string]: EffectConstructor | undefined } = {};

EffectList.forEach(element => {
    Effects[element.name] = element.constructor;
})

export { Effects };