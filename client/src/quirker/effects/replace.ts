import { Effect, StackEffect } from "..";
import { stringReplacer } from "./util";

function replace(search: string, replacement: string): StackEffect {
    return stringReplacer((text) => text.replaceAll(search, replacement))
}

function isString(s: any): s is string {
    return typeof s === "string"
}

const Replace: Effect = {
    name: "replace",
    constructor: vec => {
        let ser = vec.pop();
        let rep = vec.pop();
        if ((rep !== undefined) && (ser !== undefined)) {
            if (typeof rep === "string" && typeof ser === "string") {
                vec.push(replace(rep, ser));
            } else if (typeof rep !== "string" && typeof ser !== "string") {
                return "Both arguments were not strings."
            } else if (typeof rep !== "string" || typeof ser !== "string") {
                return "An argument was not a string."
            }
        } else {
            return "Not enough arguments for replace.";
        }
    }
}

export default Replace;