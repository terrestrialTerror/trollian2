import { Effect, StackEffect, StackNode } from "..";

function splitBy(sep: string): StackEffect {
    return (nodes) => {
        let processed: StackNode[] = [];
        for (let i = 0; i < nodes.length; i++) {
            let node = nodes[i];
            if (typeof node === "object") {
                if (node.children) {
                    let splitChildren = splitBy(sep)(node.children);
                    if (typeof splitChildren === "string") {
                        return splitChildren;
                    } else {
                        node.children = splitChildren;
                        processed.push(node);
                    }
                }
            } else {
                // 1 SHOULD ST4RT 4DD1NG COMM3NTS
                // W3'R3 R3V3RS1NG TH1S B3C4US3 
                // TH3 F1RST WORD SHOULD B3 4CC3SS4BL3
                // BY 4 POP, TH4T'S HOW 1T'S R34D OUT W1TH
                // PULLS SO W3 DO TH1S TO K33P TH3 ST4CK ORD3R
                node.split(sep).reverse().forEach(node => {
                    processed.push(node);
                })
            }
        }
        return processed;
    }
}

const SplitBy: Effect = {
    name: "splitBy",
    constructor: vec => {
        let sep = vec.pop();
        if (sep !== undefined) {
            if (typeof sep === "string") {
                vec.push(splitBy(sep));
            } else {
                return "Arguent was not a string."
            }
        } else {
            return "Not enough arguments for splitBy.";
        }
    }
}

export default SplitBy;