import { Effect, StackEffect, StackNode } from "..";

function prepend(node: StackNode): StackEffect {
    return nodes => {
        nodes.push(node);
        return nodes;
    }
}

const Prepend: Effect = {
    name: "prepend",
    constructor: vec => {
        let arg = vec.pop();
        if (arg !== undefined) {
            if (typeof arg === 'string' || typeof arg === 'object') {
                vec.push(prepend(arg));
            }
            else {
                return "Argument cannot be an effect."

            }
        } else {
            return "Not enough arguments for append.";
        }
    }
}

export default Prepend;