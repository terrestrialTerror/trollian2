import { Effect, StackEffect } from "..";

function color(color: string): StackEffect {
    return nodes => {
        switch (nodes.length) {
            case 0:
                return [];
            case 1:
                {
                    let node = nodes[0];
                    if (typeof node === "string") {
                        return [{
                            kind: 'styled',
                            style: { color: color },
                            children: [node],
                        }]
                    } else {
                        switch (node.kind) {
                            case 'styled':
                                node.style.color = color;
                                return [node];
                            default:
                                return [{
                                    kind: 'styled',
                                    style: { color: color },
                                    children: [node],
                                }]
                        }
                    }
                }
            default:
                return [{
                    kind: 'styled',
                    style: { color: color },
                    children: nodes
                }]
        }
    }
}

const Color: Effect = {
    name: "color",
    constructor: (vec) => {
        let arg = vec.pop();
        if (arg) {
            if (typeof arg === "string") {
                vec.push(color(arg));
            } else {
                return "Incorrect argument type for color.";
            }
        }
        else {
            return "Not enough arguments for color.";
        }
    }
};

export default Color;