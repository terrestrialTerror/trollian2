REGISTRY:=$(file < registry)

build: build-client build-server

build-client: 
	(podman image exists trollian2-client-build) || (podman build -t trollian2-client-build -f Containerfile.build client)
	podman run --rm -v "./client":/project trollian2-client-build
	podman build -t trollian2-client client

build-server: 
	(podman image exists trollian2-server-build) || (podman build -t trollian2-server-build -f Containerfile.build server)
	podman run --rm -v "./server":/project trollian2-server-build
	podman build -t trollian2-server server 

publish: build
	podman push localhost/trollian2-client:latest $(REGISTRY)/trollian2-client:latest
	podman push localhost/trollian2-server:latest $(REGISTRY)/trollian2-server:latest

dev-up:
	(podman pod exists trollian2-dev) || (podman pod create -p 3000:3000 -p 5432:5432 -n trollian2-dev)
	(podman image exists trollian2-client-dev) || (podman build -t trollian2-client-dev -f Containerfile.dev --restart=always client)
	(podman image exists trollian2-server-dev) || (podman build -t trollian2-server-dev -f Containerfile.dev --restart=always server)
	(podman container exists trollian2-client-dev) || (podman run -d --name trollian2-client-dev --pod trollian2-dev -v "./client":/www trollian2-client-dev)
	(podman container exists trollian2-server-dev) || (podman run -d --name trollian2-server-dev --pod trollian2-dev -v "./server":/server trollian2-server-dev)
	(podman container exists trollian2-nginx-dev) || (podman create --name trollian2-nginx-dev -v "./nginx.conf":/etc/nginx/nginx.conf:ro --pod trollian2-dev docker.io/nginx)
	podman container start trollian2-nginx-dev	
	(podman container exists trollian2-postgresql-dev) || (podman create --name trollian2-postgresql-dev -e POSTGRES_PASSWORD=password --pod trollian2-dev docker.io/postgres)
	podman container start trollian2-postgresql-dev
	{ podman logs -nf trollian2-client-dev & podman logs -nf trollian2-server-dev; }

dev-down:
	podman container stop trollian2-client-dev
	podman container rm trollian2-client-dev
	podman container stop trollian2-server-dev
	podman container rm trollian2-server-dev
	podman container stop trollian2-nginx-dev
	podman container stop trollian2-postgresql-dev

clean:
	-podman container stop trollian2-client-dev
	-podman container rm trollian2-client-dev
	-podman container stop trollian2-server-dev
	-podman container rm trollian2-server-dev
	-podman container stop trollian2-nginx-dev
	-podman container rm trollian2-nginx-dev
	-podman container stop trollian2-postgresql-dev
	-podman container rm trollian2-postgresql-dev
	-podman pod rm trollian2-dev
	-podman image rm trollian2-client
	-podman image rm trollian2-client-dev
	-podman image rm trollian2-client-build
	-podman image rm trollian2-server
	-podman image rm trollian2-server-dev
	-podman image rm trollian2-server-build
	-rm -r client/dist
	-rm -r client/node_modules
	-rm -r server/target